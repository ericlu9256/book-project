#from urllib import request
from django.shortcuts import get_object_or_404, redirect, render
from django.contrib.auth.models import User
from bookapp.models import Book, Magazine,Genre
from .forms import BookForm, MagazineForm
from django.contrib.auth.decorators import login_required

#login required decoractor 
@login_required


# Create your views here.

def show_books(request):
    books = Book.objects.all()
    context = {
        "books": books
    }
    return render(request, "books/list.html", context)




def create_book(request):
     
    context = {}
    form = BookForm(request.POST or None)
    if form.is_valid():
        book=form.save()
        return redirect("showbook",pk=book.pk)
    context ['form'] = form 
    return render(request, "books/create_book.html", context)



def show_one_book(request, pk):
    context = {
        "book": Book.objects.get(pk=pk)
    }
    return render(request, "books/detail.html", context)


def update_book(request,pk):
    context= {}
    obj= get_object_or_404(Book,pk=pk)
    form = BookForm(request.POST or None, instance = obj)
    if form.is_valid():
        book=form.save()
        return redirect("booklist")
    context ['form'] = form
    return render(request,"books/update.html",context)



#delete view
def delete_book(request, pk):
    context ={}
    obj = get_object_or_404(Book, pk=pk)
    if request.method =="POST":
        # delete object
        obj.delete()
        return redirect("booklist")
    return render(request, "books/delete.html", context)


# magazines list view login required. 
@login_required
def view_magazines(request):

    #magazines = Magazine.objects.all()
    magazines = request.user.magazines.all()
    context = {
        "magazines": magazines
    }
    return render(request, "magazines/list.html", context)



#display one magazine 

def show_one_magazine(request, pk):
    context = {
        "magazine": Magazine.objects.get(pk=pk)
    }
    return render(request, "magazines/detail.html", context)


#allowing user to add a magazine on their end 
def create_magazine(request):
     
    context = {}
    form = MagazineForm(request.POST or None)
    if form.is_valid():
        magazine=form.save()
        return redirect("showmagazine",pk=magazine.pk)
    context ['form'] = form 
    return render(request, "magazines/create.html", context)

#updating magazine on user end 
def update_magazine(request,pk):
    context= {}
    obj= get_object_or_404(Magazine,pk=pk)
    form = MagazineForm(request.POST or None, instance = obj)
    if form.is_valid():
        magazine =form.save()
        return redirect("magazinelist")
    context ['form'] = form
    return render(request,"magazines/update.html",context)


#delete magazine
def delete_magazine(request, pk):
    context ={}
    obj = get_object_or_404(Magazine, pk=pk)
    if request.method =="POST":
        # delete object
        obj.delete()
        return redirect("magazinelist")
    return render(request, "magazines/delete.html", context)


def view_genres(request):
    genres = Genre.objects.all()
    context = {
        "genres": genres
        
    }
    return render(request, "magazines/genres_list.html", context)


def show_one_genre(request, pk):
    context = {
        "genre": Genre.objects.get(pk=pk)
    }
    return render(request, "magazines/genre_detail.html", context)

def list_reviews(request):
    book_reviews = request.user.reviews.all()
    context = {
        "reviews": book_reviews
    }
    return render(request,"books/review_list.html",context)