from distutils.text_file import TextFile
from operator import mod
from platform import release
from tkinter import CASCADE
from turtle import title
from unittest.util import _MAX_LENGTH
from django.db import models
from django.contrib.auth.models import User



class  Book(models.Model):
    title = models.CharField(max_length=200, unique=True, default= "" )
    authors = models.ManyToManyField("Author", related_name="books")
    pages = models.SmallIntegerField(null=True)
    ISBN = models.BigIntegerField(null=True)
    year_published = models.SmallIntegerField(null=True)
    description = models.TextField(null=True)
    book_cover_URL = models.URLField(null=True, blank=True)
    in_print = models.BooleanField(null=True)

    def __str__(self):
        return self.title + " by " + str(self.authors.first())

class  Author(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name



class  BookReview(models.Model):
    reviewer = models.ForeignKey(User,related_name="reviews", on_delete=models.PROTECT)
    book = models.ForeignKey(Book, related_name="reviews", on_delete=models.CASCADE)
    text = models.TextField()

 
class Magazine(models.Model):
    creator = models.ForeignKey(User,related_name="magazines",on_delete=models.CASCADE)
    title = models.CharField(max_length=100,null = True)
    magazine_cover_URL = models.URLField(null = True, blank= True)
    description = models.TextField(max_length=200, null = True)
    release_cycle = models.CharField(max_length=200, null = True)
    

    def __str__(self):
        return self.title + " release cycle is  " + self.release_cycle


class Genre(models.Model):
    type = models.CharField(max_length=100)
    magazines =models.ManyToManyField("Magazine", related_name="magazines")

    def __str__(self):
        return self.type 

class Issue(models.Model):
    magazine= models.ForeignKey(Magazine,related_name="issues",on_delete=models.CASCADE)
    title =models.CharField(max_length=150)
    description=models.TextField(max_length=300, null = True)
    issue_URL = models.URLField(null = True, blank= True)
    page_count =models.SmallIntegerField(null=True)
    date=models.DateField(null=True)