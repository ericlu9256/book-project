from django import forms



try: 
    from .models import Book


    class BookForm(forms.ModelForm):
        class Meta:
            model = Book

            fields = [		
                "title",
                "authors",
			    "description",
                "book_cover_URL",
            ]
except Exception:
    pass


try: 
    from .models import Magazine


    class MagazineForm(forms.ModelForm):
        class Meta:
            model = Magazine

            fields = [		
                "title",
                "magazine_cover_URL",
			    "description",
                "release_cycle",
            ]
except Exception:
    pass