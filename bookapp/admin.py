from django.contrib import admin
from .models import Author, Book, BookReview, Genre, Issue, Magazine

admin.site.register(Book)
# Register Magazine
admin.site.register(Magazine)
admin.site.register(BookReview)
admin.site.register(Author)
admin.site.register(Genre)
admin.site.register(Issue)