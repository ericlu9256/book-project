from turtle import update
from urllib import request
from django.contrib import admin
from django.urls import path

from bookapp.views import(
    create_book,
    create_magazine,
    delete_magazine,
    show_books,
    show_one_book,
    show_one_genre,
    show_one_magazine,
    update_book,
    delete_book,
    view_magazines,
    show_one_magazine,
    create_magazine,
    update_magazine,
    delete_magazine,
    view_genres,
    show_one_genre,
    list_reviews,)

urlpatterns = [
    path('',show_books, name= "booklist"),
    path('create/', create_book, name = "create_book"),
    path("<int:pk>/", show_one_book, name="showbook"),
    path("<int:pk>/edit/", update_book, name="editbook"),
    path("<int:pk>/delete/", delete_book, name="deletebook"),
    path('magazines/', view_magazines, name = "magazinelist"),
    path("magazines/<int:pk>/", show_one_magazine, name="showmagazine"),
    path('magazines/create/', create_magazine, name = "create_magazine"),
    path("magazines/<int:pk>/edit", update_magazine, name="editmagazine"),
    path("magazines/<int:pk>/delete/", delete_magazine, name="deletemagazine"),
    path('magazines/genres/', view_genres, name = "genres_list"),
    path("magazines/genres/<int:pk>/", show_one_genre, name="showgenre"),
    path('listreviews/', list_reviews, name = "listreviews"),
    

]

